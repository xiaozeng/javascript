/**
 * @name Kaka 自动签到与Cookie更新
 * @version 1.0.0
 * @description 自动获取Cookie并进行签到
 */

(async () => {
  // 1. 获取 Cookie
  const signInPageURL = 'https://www.kaka-tech.com/Kaka/api/services/app/SignInRecord/SignInAsync';
  const signInAPIURL = 'https://www.kaka-tech.com/Kaka/api/services/app/SignInRecord/SignInAsync';
  
  // 检查是否为签到页面请求
  if ($request && $request.url.includes(signInPageURL)) {
    const requestHeaders = $request.headers;
    const currentCookie = requestHeaders['Cookie'] || requestHeaders['cookie'];
    
    if (currentCookie) {
      const saveCookie = $persistentStore.write(currentCookie, 'KakaCookie');
      if (saveCookie) {
        $notify('Kaka Cookie 获取成功', '', 'Cookie已成功保存，可以关闭此提示。');
        console.log('Kaka Cookie 保存成功: ' + currentCookie);
      } else {
        $notify('Kaka Cookie 保存失败', '', '请检查脚本配置和存储空间。');
        console.log('Kaka Cookie 保存失败');
      }
    } else {
      $notify('Kaka Cookie 获取失败', '', '请求中未包含Cookie信息。');
      console.log('Kaka Cookie 获取失败，未在请求头中找到Cookie。');
    }
    $done({});
    return;
  }

  // 2. 自动签到
  const cookie = $persistentStore.read('KakaCookie');
  
  if (!cookie) {
    $notify('Kaka 签到失败', '', '未找到有效的Cookie，请先访问签到页面以获取Cookie。');
    return;
  }

  // 签到请求配置
  const signInRequest = {
    url: signInAPIURL,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': cookie,
      'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_7 like Mac OS X)'
    },
    body: 'id=de7bcd0a-2d85-4521-9b29-c2cffc69ca62&webApiUniqueID=d9963ba0-2e3c-eb1d-9362-b4ebfb40e594'
  };

  // 发送签到请求
  $httpClient.post(signInRequest, (error, response, data) => {
    if (error) {
      $notify('Kaka 签到失败', '', '请求错误：' + error);
      console.log('Kaka 签到请求错误：' + error);
    } else {
      try {
        const result = JSON.parse(data);
        if (result.success) {
          const reward = result.result.listSignInRuleData[0];
          const giftName = reward.giftName || '未知奖励';
          $notify('Kaka 签到成功', '', `恭喜您，获得${giftName}！`);
          console.log('Kaka 签到成功，获得：' + giftName);
        } else {
          const errorMsg = result.error?.message || '未知错误';
          $notify('Kaka 签到失败', '', errorMsg);
          console.log('Kaka 签到失败：' + errorMsg);
        }
      } catch (e) {
        $notify('Kaka 签到失败', '', '响应解析失败：' + e);
        console.log('Kaka 签到响应解析失败：' + e);
      }
    }
    $done();
  });
})();
